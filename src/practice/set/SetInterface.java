package practice.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/*
 	Set --> interface
 	HashSet --> class
 	LinkedHashSet --> class
 	SortedSet --> interface
 	TreeSet --> class
 	
 	1. Order of collection is not maintained
 	2. Duplicate values are not allowed
 	3. Random access is not allowed
 	4. SortedSet sorts the element in ascending order by default
 */
public class SetInterface {

	public static void main(String[] args) {

		SetInterface si = new SetInterface();
		si.treeSet();
//		si.sortedSet();
//		si.linkedHashSet();
//		si.hashSet();
//		si.setimpli();
	}
	
	public void treeSet(){
		System.out.println("======== TreeSet implementation ========");
		TreeSet<String> treeSet = new TreeSet<String>();
		treeSet.add("o");
		treeSet.add("i");
		treeSet.add("u");
		treeSet.add("e");
		treeSet.add("o");
		treeSet.add("a");
		System.out.println("Tree set after adding elements is : "+treeSet);
		
		System.out.println(treeSet.ceiling("i")+ " is the least value element in the treeset which is greaterthan i");
		
		System.out.println(treeSet.floor("i")+ " is the highest value element in the treeset which is lessthan i");
		
		System.out.println(treeSet.first()+ " is the lowest element in the set");
		
		System.out.println(treeSet.last()+ " is the highest element in the set");
		
		System.out.println(treeSet.higher("i")+" is the least element greaterthan i");
		
		System.out.println(treeSet.lower("i")+" is the highest element lessthan i");
		
		System.out.println("Remove first element "+treeSet.pollFirst());
		
		System.out.println("Remove last element "+treeSet.pollLast());
		
		System.out.println("Elements in the tree set are : "+treeSet);
		
		Iterator<String> it = treeSet.descendingIterator();
		System.out.println("Decending iteration");
		while(it.hasNext()) System.out.print(it.next()+" ");
	}
	
	public void sortedSet(){
		System.out.println("======== SortedSet implementation ========");
		SortedSet<String> sSet = new TreeSet<String>();
		sSet.add("h");
		sSet.add("p");
		sSet.add("s");
		sSet.add("n");
		sSet.add("d");
		sSet.add("o");
		System.out.println("Sorted set items after adding : "+sSet);
		
		System.out.println("Lowest value element in the sorted set is : "+sSet.first());
		
		System.out.println("Highest value element in the sorted set is : "+sSet.last());
		
		System.out.println("HeadSet of the collection is : "+sSet.headSet("n"));
		
		System.out.println("TailSet of the collection is : "+sSet.tailSet("n"));	
	}
	
	public void linkedHashSet(){
		System.out.println("======== linkedHashSet implementation ========");
		LinkedHashSet<String> set = new LinkedHashSet<String>();
		System.out.println("Is linkedHashSet empty : "+set.isEmpty());
		
		set.add("Orange");
		set.add("banana");
		set.add("mango");
		set.add("cherry");
		set.add("mango");
		System.out.println("linkedHashSet items after adding : "+set);
		
		LinkedHashSet<String> setlinked = new LinkedHashSet<String>();
		setlinked.add("pineapple");
		setlinked.add("apple");
		setlinked.add("banana");
		System.out.println("Other linkedHashSet is : "+setlinked);
		
		set.addAll(setlinked);
		System.out.println("linkedHashSet after adding both linkedHashSet is : "+ set);
		
		String[] array = new String[set.size()];
		set.toArray(array);
		System.out.println("Items in the array are");
		for(String fruit: array) System.out.print(fruit+" ");
		
		System.out.println("\nPrinting linkedHashSet items using forEach advanced loop (java 8)");
		set.forEach(fruit -> System.out.print(fruit +" "));
		
		Iterator<String> it = set.iterator();
		System.out.println("\nPrinting linkedHashSet items using iterator");
		while(it.hasNext()) System.out.print(it.next()+" ");
		
		System.out.println("Does linkedHashSet has apple? -> "+set.contains("apple"));
		
		setlinked.clear();
		System.out.println("Is linkedHashSet empty? -> "+setlinked.isEmpty());
		setlinked.addAll(set);
		
		System.out.println("Is linkedHashSet equals setlinked? -> "+set.equals(setlinked));
		
		set.remove("cherry");
		System.out.println("linkedHashSet items after removing cherry is : "+ set);
	}
	
	public void hashSet(){
		System.out.println("======== HashSet implementation ========");
		HashSet<String> set = new HashSet<String>();
		System.out.println("Is HashSet empty : "+set.isEmpty());
		
		set.add("Orange");
		set.add("banana");
		set.add("mango");
		set.add("cherry");
		set.add("mango");
		set.add(null);
		System.out.println("HashSet items after adding : "+set);
		
		HashSet<String> setlinked = new LinkedHashSet<String>();
		setlinked.add("pineapple");
		setlinked.add("apple");
		setlinked.add("banana");
		System.out.println("Other HashSet is : "+setlinked);
		
		set.addAll(setlinked);
		System.out.println("HashSet after adding both sets is : "+ set);
		
		String[] array = new String[set.size()];
		set.toArray(array);
		System.out.println("Items in the array are");
		for(String fruit: array) System.out.print(fruit+" ");
		
		System.out.println("\nPrinting HashSet items using forEach advanced loop (java 8)");
		set.forEach(fruit -> System.out.print(fruit +" "));
		
		Iterator<String> it = set.iterator();
		System.out.println("\nPrinting HashSet items using iterator");
		while(it.hasNext()) System.out.print(it.next()+" ");
		
		System.out.println("Does HashSet has apple? -> "+set.contains("apple"));
		
		setlinked.clear();
		System.out.println("Is HashSet empty? -> "+setlinked.isEmpty());
		setlinked.addAll(set);
		
		System.out.println("Is HashSet equals setlinked? -> "+set.equals(setlinked));
		
		set.remove("cherry");
		System.out.println("HashSet items after removing cherry is : "+ set);
		
		System.out.println("Cloned set is : "+set.clone());
	}
	
	public void setimpli(){
		System.out.println("======== Set implementation using HashSet ========");
		Set<String> set = new HashSet<String>();
		System.out.println("Is set empty : "+set.isEmpty());
		
		set.add("Orange");
		set.add("banana");
		set.add("mango");
		set.add("cherry");
		set.add("mango");
		System.out.println("Set items after adding : "+set);
		
		Set<String> setlinked = new LinkedHashSet<String>();
		setlinked.add("pineapple");
		setlinked.add("apple");
		setlinked.add("banana");
		System.out.println("Other set is : "+setlinked);
		
		set.addAll(setlinked);
		System.out.println("Set after adding both sets is : "+ set);
		
		String[] array = new String[set.size()];
		set.toArray(array);
		System.out.println("Items in the array are");
		for(String fruit: array) System.out.print(fruit+" ");
		
		System.out.println("\nPrinting set items using forEach advanced loop (java 8)");
		set.forEach(fruit -> System.out.print(fruit +" "));
		
		Iterator<String> it = set.iterator();
		System.out.println("\nPrinting set items using iterator");
		while(it.hasNext()) System.out.print(it.next()+" ");
		
		System.out.println("Does Set has apple? -> "+set.contains("apple"));
		
		setlinked.clear();
		System.out.println("Is set empty? -> "+setlinked.isEmpty());
		setlinked.addAll(set);
		
		System.out.println("Is set equals setlinked? -> "+set.equals(setlinked));
		
		set.remove("cherry");
		System.out.println("Set items after removing cherry is : "+ set);
	}
}
