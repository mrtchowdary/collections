package practice.list;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

/*
 	List --> interface
 	ArrayList --> class
 	LinkedList --> class
 	Vector --> class
 	stack --> class
 	
	1. Ordered collections of elements
	2. Duplicates allowed
	3. Access elements based on index
 */

public class ListInterface {

	public static void main(String[] args) {
		
		ListInterface li = new ListInterface();
		li.listArrayList();
//		li.listLinkedList();
//		li.arrayList();
//		li.linkedList();
//		li.vector();
//		li.stack();
	}
		
	public void listArrayList(){
		System.out.println("======== list implementation using List ========");
		List<String> list = new ArrayList<String>();
		System.out.println("Is List empty : "+list.isEmpty());
		list.add("apple");
		list.add("mango");
		list.add("grapes");
		list.add("orange");
		list.add("papaya");
		System.out.println("Items in the list are : "+list);
		
		list.add(2,"pineapple");
		list.add("banana");
		list.add(list.size(),"cherry");
		System.out.println("Items in the list after adding few more : "+list);
		
		list.set(5, "gauva");
		System.out.println("Items in the list after replacing 5th is : "+list);
		
		Collections.rotate(list, 5);
		System.out.println("List items on rotating by 5 is : "+list);
		
		List<String> subList = list.subList(4, 7);
		System.out.println("Sub-list of list from index 4 to 7 is : "+subList);
		
		list.addAll(0, subList);
		System.out.println("List after adding the sublist at the start of list is : "+list);
		
		Iterator<String> it = list.iterator();
		System.out.println("Print list items using iterator : ");
		while(it.hasNext()){
			System.out.print(it.next()+" ");
		}
		
		System.out.println("\nPrint list items using for each loop : ");
		for(String fruit:list){
			System.out.print(fruit+" ");
		}
		
		System.out.println("\nPrint list items using advanced foreach(java 8) : ");
		list.forEach(fruit -> System.out.print(fruit+" "));
		
		System.out.println("Max item in the list is : "+Collections.max(list));
		
		System.out.println("Min item in the list is : "+Collections.max(list));
		
		Collections.sort(list);
		System.out.println("List on sorting is : "+ list);
		
		System.out.println("Index of (first occurance) list item cherry is : "+list.indexOf("cherry"));
		
		System.out.println("Last index of (last occurance) list item cherry is : "+list.lastIndexOf("cherry"));
		
		String[] array = new String[list.size()];
		list.toArray(array);
		System.out.println("printing array elements --> "+Arrays.toString(array));
		
		list.removeIf(item -> item.equals("cherry"));
		System.out.println("List items after removeif condition is : "+list);
		
		list.remove(list.size()-1);
		System.out.println("List after removing sub list items is : "+list);
		
		List<String> dupList = new ArrayList<String>();
		dupList.addAll(list);
		System.out.println("Items in dummy list after addig all items from list is : "+dupList);
		
		
		list.clear();
		System.out.println("Is list empty after clearing : "+list.isEmpty());
	}
	
	public void listLinkedList(){
		System.out.println("======== list implementation using LinkedList ========");
		List<String> list = new LinkedList<String>();
		System.out.println("Is List empty : "+list.isEmpty());
		list.add("apple");
		list.add("mango");
		list.add("grapes");
		list.add("orange");
		list.add("papaya");
		System.out.println("Items in the list are : "+list);
		
		list.add(2,"pineapple");
		list.add("banana");
		list.add(list.size(),"cherry");
		System.out.println("Items in the list after adding few more : "+list);
		
		list.set(5, "gauva");
		System.out.println("Items in the list after replacing 5th is : "+list);
		
		Collections.rotate(list, 5);
		System.out.println("List items on rotating by 5 is : "+list);
		
		List<String> subList = list.subList(4, 7);
		System.out.println("Sub-list of list from index 4 to 7 is : "+subList);
		
		list.addAll(0, subList);
		System.out.println("List after adding the sublist at the start of list is : "+list);
		
		Iterator<String> it = list.iterator();
		System.out.println("Print list items using iterator : ");
		while(it.hasNext()){
			System.out.print(it.next()+" ");
		}
		
		System.out.println("\nPrint list items using for each loop : ");
		for(String fruit:list){
			System.out.print(fruit+" ");
		}
		
		System.out.println("\nPrint list items using advanced foreach(java 8) : ");
		list.forEach(fruit -> System.out.print(fruit+" "));
		
		System.out.println("Max item in the list is : "+Collections.max(list));
		
		System.out.println("Min item in the list is : "+Collections.max(list));
		
		Collections.sort(list);
		System.out.println("List on sorting is : "+ list);
		
		System.out.println("Index of (first occurance) list item cherry is : "+list.indexOf("cherry"));
		
		System.out.println("Last index of (last occurance) list item cherry is : "+list.lastIndexOf("cherry"));
	}
	
	public void arrayList(){
		System.out.println("======== ArrayList implementation ========");
		ArrayList<String> list = new ArrayList<String>();
		System.out.println("Is ArrayList empty : "+list.isEmpty());
		list.add("apple");
		list.add("mango");
		list.add("grapes");
		list.add("orange");
		list.add("papaya");
		System.out.println("Items in the ArrayList are : "+list);
		
		list.add(2,"pineapple");
		list.add("banana");
		list.add(list.size(),"cherry");
		System.out.println("Items in the ArrayList after adding few more : "+list);
		
		list.set(5, "gauva");
		System.out.println("Items in the list after replacing 5th is : "+list);
		
		Collections.rotate(list, 5);
		System.out.println("ArrayList items on rotating by 5 is : "+list);
		
		List<String> subList = list.subList(4, 7);
		System.out.println("Sub-list of ArrayList from index 4 to 7 is : "+subList);
		
		list.addAll(0, subList);
		System.out.println("ArrayList after adding the sublist at the start of list is : "+list);
		
		Iterator<String> it = list.iterator();
		System.out.println("Print ArrayList items using iterator : ");
		while(it.hasNext()){
			System.out.print(it.next()+" ");
		}
		
		System.out.println("\nPrint ArrayList items using for each loop : ");
		for(String fruit:list){
			System.out.print(fruit+" ");
		}
		
		System.out.println("\nPrint ArrayList items using advanced foreach(java 8) : ");
		list.forEach(fruit -> System.out.print(fruit+" "));
		
		System.out.println("Max item in the ArrayList is : "+Collections.max(list));
		
		System.out.println("Min item in the ArrayList is : "+Collections.max(list));
		
		Collections.sort(list);
		System.out.println("ArrayList on sorting is : "+ list);
		
		System.out.println("Index of (first occurance) list item cherry is : "+list.indexOf("cherry"));
		
		System.out.println("Last index of (last occurance) list item cherry is : "+list.lastIndexOf("cherry"));
	}

	public void linkedList(){
		System.out.println("======== Linkedlist implementation ========");
		List<String> list = new LinkedList<String>();
		System.out.println("Is Linkedlist empty : "+list.isEmpty());
		list.add("apple");
		list.add("mango");
		list.add("grapes");
		list.add("orange");
		list.add("papaya");
		System.out.println("Items in the Linkedlist are : "+list);
		
		list.add(2,"pineapple");
		list.add("banana");
		list.add(list.size(),"cherry");
		System.out.println("Items in the Linkedlist after adding few more : "+list);
		
		list.set(5, "gauva");
		System.out.println("Items in the Linkedlist after replacing 5th is : "+list);
		
		Collections.rotate(list, 5);
		System.out.println("Linkedlist items on rotating by 5 is : "+list);
		
		List<String> subList = list.subList(4, 7);
		System.out.println("Sub-list of Linkedlist from index 4 to 7 is : "+subList);
		
		list.addAll(0, subList);
		System.out.println("Linkedlist after adding the sublist at the start of list is : "+list);
		
		Iterator<String> it = list.iterator();
		System.out.println("Print Linkedlist items using iterator : ");
		while(it.hasNext()){
			System.out.print(it.next()+" ");
		}
		
		System.out.println("\nPrint Linkedlist items using for each loop : ");
		for(String fruit:list){
			System.out.print(fruit+" ");
		}
		
		System.out.println("\nPrint Linkedlist items using advanced foreach(java 8) : ");
		list.forEach(fruit -> System.out.print(fruit+" "));
		
		System.out.println("Max item in the Linkedlist is : "+Collections.max(list));
		
		System.out.println("Min item in the Linkedlist is : "+Collections.max(list));
		
		Collections.sort(list);
		System.out.println("Linkedlist on sorting is : "+ list);
		
		System.out.println("Index of (first occurance) Linkedlist item cherry is : "+list.indexOf("cherry"));
		
		System.out.println("Last index of (last occurance) Linkedlist item cherry is : "+list.lastIndexOf("cherry"));
	}
	
	public void vector(){
		System.out.println("======== vector implementation ========");
		Vector<String> vector = new Vector<String>();
		System.out.println("Is vector empty : "+vector.isEmpty());
		System.out.println("initial capacity of vector is : "+vector.capacity());
		vector.add("mango");
		vector.addElement("apple");
		vector.add("pineapple");
		vector.addElement("gauva");
		System.out.println("Vector after adding items is : "+vector);
		
		vector.add(1, "orange");
		vector.insertElementAt("papaya", 4);
		vector.set(3, "cherry");   			//replaces item at specified index
		vector.add("banana");
		System.out.println("Vector after changes is : "+ vector);
		
		System.out.println("Printing vector using advanced foreach(java 8)");
		vector.forEach(item -> System.out.print(item+" "));
		
		Enumeration<String> enumerate = vector.elements();
		System.out.println("\nPrinting vector items using enumeration");
		while(enumerate.hasMoreElements()) System.out.print(enumerate.nextElement()+" ");

		Iterator<String> it = vector.iterator();
		System.out.println("\nPrinting vector items using iterator");
		while(it.hasNext()) System.out.print(it.next()+" ");
		
		Vector<String> newVector = (Vector<String>) vector.clone();
		System.out.println("Cloned vector is : "+newVector);
		
		Collections.sort(vector);
		System.out.println("Vector after sorting is : " + vector);
		
		vector.removeIf(item -> item.equals("apple"));
		System.out.println("Vector items after removeif is : "+vector);
	}
	
	//uses vector method as stack extends vector
	public void stack(){
		Stack<String> stack = new Stack<String>();
		stack.push("banana");
		stack.push("pineapple");
		stack.add("gauva");
		System.out.println("Stack after adding few items is : "+stack);
		
		System.out.println("Position of pineapple in the stack is : "+ stack.search("pineapple"));
		
		System.out.println("Top most item in the stack is : "+stack.peek());
		
		System.out.println("Remove top most item "+stack.pop()+" removed from the stack");
		
		System.out.println("Stack after removing the top most item is : "+ stack);
		
	}
}
