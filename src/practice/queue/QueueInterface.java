package practice.queue;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.prefs.BackingStoreException;

public class QueueInterface {

	public static void main(String[] args) {
		
		QueueInterface qi = new QueueInterface();
		qi.arrayDeque();
//		qi.deque();
//		qi.priorityQueue();
//		qi.queueImpli();
	}
	
	public void arrayDeque(){
		ArrayDeque<String> adeque = new ArrayDeque<String>();
		adeque.addLast("cherry");
		System.out.println(adeque);
		//Supports all methods of deque
	}
	
	public void deque(){
		Deque<String> dequeue = new ArrayDeque<String>();
		dequeue.offer("orange");
		dequeue.offer("pineapple");
		dequeue.offer("cherry");
		dequeue.offer("gauva");
		System.out.println("Deque after adding elements is : "+dequeue);
		
		dequeue.addFirst("mango");
		dequeue.addLast("watermelon");
		dequeue.offerFirst("apple");
		dequeue.offerLast("grapes");
		dequeue.push("cherry");
		System.out.println("Deque elements after adding elements with all methods is : "+dequeue);
		
		Iterator<String> it = dequeue.iterator();
		System.out.println("Printing Deque elements using iterator");
		while(it.hasNext()) System.out.print(it.next()+" ");
		
		Iterator<String> dit = dequeue.descendingIterator();
		System.out.println("\nPrinting dqueue elements using decending iterator");
		while(dit.hasNext()) System.out.print(dit.next()+" ");
		
		System.out.println("\nGet first element using getFirst(): "+dequeue.getFirst());
		
		System.out.println("Get last element using getLast() : "+dequeue.getLast());
		
		System.out.println("Retrive and remove first element using pollFirst() : "+dequeue.pollFirst());
		
		System.out.println("Retrive and remove last element using pollLast() : "+dequeue.pollLast());
		
		System.out.println("Get last element using peekFirst(): "+dequeue.peekFirst());
		
		System.out.println("Get last element using peekLast(): "+dequeue.peekLast());
		
		System.out.println("Retrive and removes first element using pop()"+dequeue.pop());
		
		System.out.println("Retrive and remove first element using removeFirst()"+dequeue.removeFirst());
		
		System.out.println("Retrive and remove last element using removeLast()"+dequeue.removeLast());
		
		System.out.println("Deque elements after all operations are : "+dequeue);
	}
	
	public void priorityQueue(){
		PriorityQueue<String> queue = new PriorityQueue<String>();
		queue.add("mango");
		queue.offer("orange");
		queue.add("apple");
		queue.offer("gauva");
		System.out.println("Priority Queue elements after adding is :"+queue);
		
		PriorityQueue<String> priQueue = new PriorityQueue<String>();
		priQueue.offer("lichies");
		priQueue.offer("watermelon");
		priQueue.offer("cherry");
		System.out.println("New Priority Queue elements after adding is :"+priQueue);
		
		queue.addAll(priQueue);
		System.out.println("Priority Queue elements after adding both queues are : "+queue);
		
		System.out.println("Retrive the first element in the Priority queue : "+queue.peek());
		
		System.out.println("Retrive and remove the first element in the Priority queue using poll : "+queue.poll());
		
		System.out.println("Remove the first element in the Priority queue using remove : "+queue.remove());
		
		System.out.println("Priority queue after all operations is : "+queue);
	}
	
	public void queueImpli(){
		Queue<String> queue = new LinkedList<String>();	//new PriorityQueue<String>();
		queue.offer("mango");
		queue.offer("orange");
		queue.offer("apple");
		queue.offer("gauva");
		System.out.println("Queue (LinkedList) elements after adding is :"+queue);
		
		Queue<String> priQueue = new PriorityQueue<String>();
		priQueue.offer("lichies");
		priQueue.offer("watermelon");
		priQueue.offer("cherry");
		System.out.println("Queue (Priority Queue) elements after adding is :"+priQueue);
		
		Queue<String> arrQueue = new ArrayDeque<String>();
		arrQueue.offer("papaya");
		arrQueue.offer("banana");
		arrQueue.offer("apple");
		System.out.println("Queue (ArrayDeque) elements after adding is :"+arrQueue);
		
		queue.addAll(arrQueue);
		queue.addAll(priQueue);
		System.out.println("Queue elements after adding all queues are : "+queue);
		
		String[] array = new String[queue.size()];
		queue.toArray(array);
		System.out.println("Array elements are : "+Arrays.toString(array));
		
		System.out.println("Queue elements before : "+queue);
		
		System.out.println("Retrive the first element in the queue : "+queue.peek());
		
		System.out.println("Retrive and remove the first element in the queue using poll : "+queue.poll());
		
		System.out.println("Remove the first element in the queue using remove : "+queue.remove());
		
		System.out.println("Queue after all operations is : "+queue);
	}

}
