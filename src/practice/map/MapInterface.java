package practice.map;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class MapInterface {

	public static void main(String[] args) {

		MapInterface mi = new MapInterface();
		mi.treeMap();
//		mi.linkedHashMap();
//		mi.hashMap();
//		mi.mapImpli();
	}
	
	public void treeMap(){
		TreeMap<Integer, String> map = new TreeMap<Integer, String>();
		map.put(33, "Ramu");
		map.put(29, "Lakshmi");
		map.put(4, "Ishita");
		map.put(2, "satya");
		System.out.println("TreeMap elements after inserting is : "+map);
	}
	
	public void linkedHashMap(){
		LinkedHashMap<Integer, String> map = new LinkedHashMap<Integer, String>();
		map.put(33, "Ramu");
		map.put(29, "Lakshmi");
		map.put(4, "Ishita");
		map.put(2, "satya");
		System.out.println("LinkedHashMap elements after inserting is : "+map);
	}
	
	public void hashMap(){
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		map.put(33, "Ramu");
		map.put(29, "Lakshmi");
		map.put(4, "Ishita");
		map.put(2, "satya");
		System.out.println("HashMap elements after inserting is : "+map);
	}

	public void mapImpli(){
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(33, "Ramu");
		map.put(29, "Lakshmi");
		map.put(4, "Ishita");
		map.put(2, "satya");
		System.out.println("Map elements after inserting is : "+map);
		
		map.putIfAbsent(58, "Satya");
		map.putIfAbsent(50, "Rani");
		map.putIfAbsent(33, "Ramu");
		System.out.println("Map elements after few more insertions is : "+map);
		
		Map<Integer, String> map2 = new LinkedHashMap<Integer, String>();
		map2.putIfAbsent(32, "Suresh");
		map2.putIfAbsent(50, "Rani");
		map2.putIfAbsent(28, "Teja");
		map2.putIfAbsent(4, "Ishita");
		map2.putIfAbsent(3, "Shanu");
		System.out.println("Elements in Map2 are : "+map2);
		
		map.putAll(map2);
		System.out.println("Map items after adding map2 is : "+map);
		
		System.out.println("Map Entry set values are : "+map.entrySet());
		
		System.out.println("Map Key set is : "+map.keySet());
		
		System.out.println("Map value set is : "+map.values());
		
		System.out.println("To get value of a key 33 in the Map is : "+map.get(33));
		
		System.out.println("Replacing value for key 2 \""+map.replace(2, "Satya mukund")+ "\" with \"Satya mukund\"");
		
		line();
		
		Set<Entry<Integer, String>> set = map.entrySet();
		Iterator<Entry<Integer, String>> it = set.iterator();
		System.out.println("Printing map values using iteration using entrySet()");
		while(it.hasNext()){ 
			Map.Entry<Integer, String> in = (Map.Entry<Integer, String>) it.next();
			System.out.println("Age : "+ in.getKey()+ " Name : "+in.getValue());
		}
		
		line();
		
		Iterator<Integer> itr = map.keySet().iterator();
		System.out.println("Printing map values using iteration using keySet()");
		while(itr.hasNext()){ 
			Integer inte = itr.next();
			System.out.println("Age : "+ inte+ " Name : "+map.get(inte));
		}
		
		line();
		
		System.out.println("Printing map values using map.keySet() in generic style");
		for(Integer ke: map.keySet())
			System.out.println("Age : "+ ke+ " Name : "+map.get(ke));
		
		line();
		
		System.out.println("Printing map values using map.entrySet() in generic style");
		for(Map.Entry<Integer, String> ent: map.entrySet()) 
			System.out.println("Age : "+ ent.getKey()+ " Name : "+ent.getValue());
		
		line();
		
		System.out.println("Printing map in ascending order of key using comparingByKey()");
		map.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		
		line();
		
		System.out.println("Printing map in decending order of value using comparingByValue() (reverse order)");
		map.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder())).forEach(System.out::println);
		
		line();
	}

	public static void line(){
		System.out.println("======================================================================");
	}
}
